%%%%%%%% correspondance 1 et 2


% am1:
% 472;136
% 481;236
% 441;186
% 497;153 %496 152


%465;249
%496;202

% am2:
% 40;136 top building angle
% 45;236 accrcoche anneau
% 7;187 coin inferieur gauche fenetre 1er etage
% 64;154 coin sup droit building %64 152

%40;249sÚparation de l'onmbre et du pas ombre du bas gauche du pilonne du
%pont
%35;204

%%%%%%%% correspondance 2 et 3

% am2
% 426 223 bas gauche fenetre
% 388 172 coin bas droite fenetre bleue gauche
% 473 128 fae maison oeil de boeuf
% 493 203 coin bas droite store devanture

%488 243 coin bas ombre bateau a droite
%437 107 coin gauche batiment devant l'arbre

% am3
% 26 222
% 7 171
% 94 130
% 110 203

%105 243
%59 107

cor12_am1 = [472 441 481 497 486; 136 186 236 153 139; 1 1 1 1 1];
cor12_am2 = [40 7 45 64 54; 136 187 236 154 141; 1 1 1 1 1];

cor23_am2 = [437 388 473 488; 107 172 128 243; 1 1 1 1];
cor23_am3 = [59 7 94 105; 107 171 130 243; 1 1 1 1];

am1 = double(imread('Amst-1.jpg'))/255;
am2 = double(imread('Amst-2.jpg'))/255;
am3 = double(imread('Amst-3.jpg'))/255;
% imagesc(am1)
% imagesc(am2)
% imagesc(am3)
bbox = [-400 800 -100 500];

H_1to2 = homography2d(cor12_am1, cor12_am2)
H_2to2 = eye(3)
H_3to2 = homography2d(cor23_am3, cor23_am2);

im_1in2 = vgg_warp_H(am1, H_1to2, 'linear', bbox);
im_2in2 = vgg_warp_H(am2, H_2to2, 'linear', bbox);
im_3in2 = vgg_warp_H(am3, H_3to2, 'linear', bbox);
% imagesc(im_1in2)
im_12in2 = max(im_1in2, im_2in2);
im_23in2 = max(im_2in2, im_3in2);
im_123in2 = max(im_12in2, im_23in2);
%pas beau mais ca marche 
imagesc(im_123in2)

